export TZ='Asia/Tokyo'

# some more aliases
alias ll='ls -l'
alias lla='ls -la'
alias llh='ls -lh'
alias llha='ls -lha'
alias la='ls -A'
alias l='ls -CF'
alias h='history'
alias c='clear'
alias t='toilet'
alias s='screen'
alias r='read'
alias q='exit'

alias d='docker'
alias dc='docker-compose'
alias dtty='docker exec -it'

alias clc='free;echo "3" | sudo tee /proc/sys/vm/drop_caches;free'
alias mkpw='cat /dev/urandom | tr -dc 'a-zA-Z0-9\-\_' | fold -w 12 | head -n 3'

# trans
alias trans2ja='trans -b :ja '
alias trans2en='trans -b :en '

# column
alias csv='column -t -s ","'

export HISTTIMEFORMAT='[%F %T] '
export EDITOR='/usr/bin/vim'

PATH="$PATH":~/bin/
PATH="$PATH":/usr/sbin

# svn
alias svnlog='svn log | less'

# Imagick aliases  DISPLAY=:99 capture example.png
alias capture='import -windows root'

# Go
# export GOPATH=$HOME/go
# export PATH=$PATH:$GOPATH/bin

# bash_completion (Mac用)
# if [ -f /usr/local/etc/profile.d/bash_completion.sh ]; then
#     . /usr/local/etc/profile.d/bash_completion.sh
# fi

# bash_completion (debian用)
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Ctrl + S スクリーンロックを無効化
stty stop undef

PS1='\[\033[1;36m\]\u@\h:\[\e[0m\]\w \$ '

# 右プロンプト
function __command_rprompt() {
    local rprompt="$(date '+%Y/%m/%d %H:%M:%S')"
    local num=$(($COLUMNS - ${#rprompt} - 2))

    printf "%${num}s$rprompt\\r" ''
}

function get_args2txt {
    if [ -p /dev/stdin ]; then
        text=`cat -`
    else
        if [[ "${1}" == "" ]]; then
            echo "argument not found."
        else
            text=$*
        fi
    fi

    echo ${text}
}

# URLエンコード・デコード
function urlencode {
    echo `get_args2txt $*` | nkf -WwMQ | tr = %
}
function urldecode {
    echo `get_args2txt $*` | nkf -w --url-input
}

# base64エンコード・デコード
function base64encode {
    echo `get_args2txt $*` | openssl enc -e -base64
}
function base64decode {
    echo `get_args2txt $*` | openssl enc -d -base64
}


# chrome search from terminal
ggr(){
    options=()
    options+=("-www.sejuku.net")
    options+=("-techacademy.jp")
    options+=("-blog.codecamp.jp")
    keywords=`urlencode $*`
    keywords+=' '`urlencode ${options[@]}`
    /usr/bin/google-chrome https://www.google.co.jp/search?q="$keywords"
}


PROMPT_COMMAND=__command_rprompt

echo -e '\e[1;32m.bash_profile loaded. \e[m'

# aptitude -h | tail -n 1 | sed -e 's/ //g' | sed -e 's/aptitude/Debian/g' | sed -e 's/ありません/ありませんよ/g' | lolcat
# echo ""
# aptitude -v moo
# aptitude -vv moo
# aptitude -vvv moo
# aptitude -vvvv moo
# aptitude -vvvvv moo | head -n 1
# apt-get moo moo moo | lolcat
# echo ""
# date | lolcat

touch $HOME/todo.txt
TODO=`cat $HOME/todo.txt`

if [[ "" != $TODO  ]]; then
  echo "============ ToDo ============"
  echo "${TODO}"
  echo "=============================="
fi
